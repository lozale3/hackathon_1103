import { LitElement, html } from 'lit-element';
import '../persona-ficha-listado/persona-ficha-listado.js';
import '../persona-form/persona-form.js';


class PersonaMain extends LitElement {


    static get properties(){
        return{
            people: {type: Array},
            showPersonForm: {type: Boolean}
        };
    }

    constructor(){
        super();
        this.getPersonData();
/*
        this.people = [
            {
                id: "1",
                dni: "12345678-z",
                name: "Jose López",
                course: "6º B",
                phone: 696999999,
                email: "pru@gmail.com"
            },
            {
                id: "2",
                dni: "55555555-A",
                name: "María Sánchez",
                course: "6º A",
                phone: 696999888,
                email: "pupil2@gmail.com"
            }
        ];
*/
        this.showPersonForm = false;

    }

    render(){
       return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <h2 class="text-center text-white">Viaje fin de curso 2021</h2>
            <br />
            <div class="row" id="peopleList">
                <div class="row row-cols-1 row-cols-sm-4">
                ${this.people.map(
                    person =>  html`<persona-ficha-listado 
                    destiny="${person.destiny}" 
                    dni="${person.dni}" 
                    name="${person.name}" 
                    course="${person.course}"
                    phone="${person.phone}"
                    email="${person.email}"
                    @delete-person="${this.deletePerson}"
                    @info-person="${this.infoPerson}"
                    >
                    </persona-ficha-listado>`
                )}
                </div>
            </div>   
            <div class="row">
                    <persona-form 
                    @persona-form-close="${this.personFormClose}" 
                    @persona-form-store="${this.personFormStore}" 
                    
                    class="d-none border rounded border-primary" id="personForm"></persona-form>
            </div>
            
       `; 
    }

    updated(changedProperties){
        console.log("updated");

        if (changedProperties.has("showPersonForm")){
            console.log("Ha cambiado el valor de la propiedad showPersonForm en persona-main");

            // cambiar valor de class d-none de persona-form

            if (this.showPersonForm === true){
                this.showPersonFormData();
            }
            else {
                this.showPersonList();
            }
        }

        if (changedProperties.has("people")){
            console.log("Ha cambiado el valor de la propiedad people en persona-main");

            this.dispatchEvent(new CustomEvent("updated-people", {
                "detail": {
                    people: this.people
                }
            }
            
        ));
    }

    }

    getPersonData(){
        console.log("getPersonData");
        console.log("Obteniendo datos de los alumnos");

        let xhr = new XMLHttpRequest();

        xhr.onload = () => {
            // Estamos contemplando el 200, pero estoy hay que consesuarlo con el BACK
            if (xhr.status === 200){
                console.log("Petición completada correctamente");
                let APIResponse = JSON.parse(xhr.responseText);
                console.log(APIResponse);
                // Esto va a cambiar en Hackathon
                this.people = APIResponse;
            }
        };
        // Aquí pondremos la url que nos digan los del BACK
        xhr.open("GET", "http://localhost:8081/apitechu/v3/students");
        xhr.send();
        console.log("Fin de getPersonData");

    }
    

    showPersonFormData(){
        console.log("showPersonFormData");
        console.log("Mostrando formulario de persona");

        this.shadowRoot.getElementById("personForm").classList.remove("d-none");
        this.shadowRoot.getElementById("peopleList").classList.add("d-none");
    }

    showPersonList(){
        console.log("showPersonList");
        console.log("Mostrando listado de personas");

        this.shadowRoot.getElementById("peopleList").classList.remove("d-none");
        this.shadowRoot.getElementById("personForm").classList.add("d-none");
    }


    // borramos personas, con funcion filter cuya salida igualamos a array inicial

    deletePerson(e){
        console.log("deletePerson en persona-main");
        console.log("Se va a borrar la persona de nombre " + e.detail.name);

        this.people = this.people.filter(
            person => person.dni != e.detail.dni

            );    

            var xhr = new XMLHttpRequest();
            let url = "http://localhost:8081/apitechu/v3/students/" + e.detail.dni;
            xhr.open("DELETE", url, true);
            xhr.send();

    }

    personFormClose(){
        console.log("personFormClose");
        console.log("Se ha cerrado el formulario de la persona");
        this.showPersonForm = false;
    }

    personFormStore(e){
        console.log("personFormStore");
        console.log("Se va a almacenar una persona");

        console.log("La propiedad destiny en person vale " + e.detail.person.destiny);
        console.log("La propiedad dni en person vale " + e.detail.person.dni);
        console.log("La propiedad name en person vale " + e.detail.person.name);
        console.log("La propiedad course en person vale " + e.detail.person.course);
        console.log("La propiedad phone en person vale " + e.detail.person.phone);
        console.log("La propiedad email en person vale " + e.detail.person.email);

        console.log("La propiedad editingPerson vale " + e.detail.editingPerson);

        if (e.detail.editingPerson === true){
            console.log("Se va a actualizar la persona con dni " + e.detail.person.dni);
  
            this.people = this.people.map(
                person => person.dni === e.detail.person.dni 
                            ? person = e.detail.person : person
            );

            var xhr = new XMLHttpRequest();
            let url = "http://localhost:8081/apitechu/v3/students/" + e.detail.person.dni
            xhr.open("PUT", url, true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify(e.detail.person));

              
        } else {
            console.log("Vamos a crear una persona");
            this.people = [...this.people, e.detail.person];
            console.log(this.people)

            var xhr = new XMLHttpRequest();
            xhr.open("POST", "http://localhost:8081/apitechu/v3/students", true);
            xhr.setRequestHeader('Content-Type', 'application/json');
            xhr.send(JSON.stringify(e.detail.person));

//            xhr.open("POST", "http://localhost:8081/apitechu/v3/students");
//            xhr.send(this.people);
    

            //            this.people.push(e.detail.person);
        }

        this.showPersonForm = false;

    }

    infoPerson(e){
        console.log("moreInfo");
        console.log("Se ha pedido más información de la persona con dni " + e.detail.dni);

        let chosenPerson = this.people.filter(
            person => person.dni === e.detail.dni
        );
        console.log(chosenPerson);

        let personToShow = {};
        personToShow.destiny = chosenPerson[0].destiny;
        personToShow.dni = chosenPerson[0].dni;
        personToShow.name = chosenPerson[0].name;
        personToShow.course = chosenPerson[0].course;
        personToShow.phone = chosenPerson[0].phone;
        personToShow.email = chosenPerson[0].email;


        this.shadowRoot.getElementById("personForm").person = personToShow;
        this.shadowRoot.getElementById("personForm").editingPerson = true;
        this.showPersonForm = true;
        
    }

}

customElements.define('persona-main', PersonaMain)