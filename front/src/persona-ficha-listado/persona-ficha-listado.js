import { LitElement, html } from 'lit-element';

class PersonaFichaListado extends LitElement {

    static get properties(){
        return{
            dni: {type: String},
            name: {type: String},
            course: {type: String},
            phone: {type: Number},
            email: {type: String},
            destiny: {type: String}
        };
    }

    constructor(){
        super();
    }

    //<img src="${this.photo.src}" height="250" width="250" alt="${this.photo.alt}">    
/*
    <ul class="list-group list-group-flush" onload="${this.translateDestination}">
    <li class="list-group-item">Destino elegido ${this.nameDestination}</li>
    </ul>
*/
    render(){
       return html`
       <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div class="card h-100">   

                <div class="card-body">
                    <h5 class="card-text"">Destino elegido ${this.destiny}</h5>
                    <h5 class="card-text">${this.dni}</h5>
                    <h5 class="card-text">${this.name}</h5>
                    <h5 class="card-text">${this.email}</h5>
                    <h5 class="card-text">${this.course}</h5>
                    <h5 class="card-text">${this.phone}</h5>
                    <h5 class="card-text">${this.email}</h5>
                </div>
                <div class="card-footer">
                    <button @click="${this.deletePerson}" class="btn btn-danger col-5"><strong>Borrar</strong></button>
                    <button @click="${this.moreInfo}" class="btn btn-info col-5 offset-1"><strong>Editar</strong></button>
                </div>
            </div>
       `; 
    }

    deletePerson(e){
        console.log("deletePerson");
        console.log("Se va a borrar la persona con dni " + this.dni);

        this.dispatchEvent(
            new CustomEvent("delete-person", {
                detail: {
                    "dni" : this.dni
                }
            })
        );
    }

    moreInfo(e){
        console.log("moreInfo");
        console.log("Se ha pedido más información de la persona " + this.name + "con dni " + this.dni);
        
        this.dispatchEvent(
            new CustomEvent("info-person", {
                detail: {
                    "dni" : this.dni
                }
            })
        );
      
    }
 /*   
    translateDestination() {
        console.log ("translateDestination");

        if (this.destiny===1) {
            this.nameDestination = "Playa"
        } else {
            this.nameDestination = "Montaña"
        };
    }
*/
}

customElements.define('persona-ficha-listado', PersonaFichaListado)