import { LitElement, html } from 'lit-element';

class PersonaForm extends LitElement {

    static get properties(){
        return{
            person: {type: Object},
            editingPerson: {type: Boolean}
        };
    }

    constructor(){
        super();

        this.resetFormData();
        this.editingPerson = false;
    }

    render(){
       return html`
            <link href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta2/dist/css/bootstrap.min.css" rel="stylesheet" integrity="sha384-BmbxuPwQa2lc/FVzBcNJ7UAyJxM6wuqIj61tLrc4wSX0szH/Ev+nYRRuWlolflfl" crossorigin="anonymous">
            <div>
                <form>
                    <div class="form-group">                                    
                        <label for="destino">Elige un destino </label>
                        <select  name="destino" id="destino" @input="${this.updateDestiny}">
                            <optgroup>
                                <option value="Playa">Playa</option>
                                <option value="Montaña">Montaña</option>
                            </optgroup>
                        </select>
                    </div>
                    <br />
                    <div class="form-group">
                        <label>DNI/NIE</label>
                        <input type="text" 
                            @input="${this.updateDni}" 
                            .value="${this.person.dni}" 
                            ?disabled="${this.editingPerson}"
                            class="form-control" 
                            placeholder="DNI/NIE" />
                    </div>
                    <br />
                    <div class="form-group">
                        <label>Nombre y Apellidos</label>
                        <input type="text" @input="${this.updateName}" .value="${this.person.name}" class="form-control" placeholder="Nombre y apellidos" rows="5" />
                    </div>
                    <br />
                    <div class="form-group">
                        <label>Curso</label>
                        <input type="text" @input="${this.updateCourse}" .value="${this.person.course}" class="form-control" placeholder="Curso" rows="5" />
                    </div>
                    <div class="form-group">
                        <label>Teléfono</label>
                        <input type="text" @input="${this.updatePhone}" .value="${this.person.phone}" class="form-control" placeholder="Teléfono" rows="5" />
                    </div>
                    <br />
                    <div class="form-group">
                        <label>email</label>
                        <input type="email" @input="${this.updateEmail}" .value="${this.person.email}" class="form-control" placeholder="Email" rows="5" />
                    </div>
                    <br />
                    
                    <button @click="${this.goBack}" class="btn btn-primary"><strong>Atrás</strong></button>
                    <button @click="${this.storePerson}" class="btn btn-success"><strong>Guardar</strong></button>
                </form>
            </div>
       `; 
    }
/*
    selectDestination () {
        console.log ("selectDestination: vamos a preseleccionar el comobo correcto");

        if (this.person.destiny === 1) {
            document.getElementById("destino").selectedValue=1
        } else {
            document.getElementById("destino").selectedValue=2
        };
    }
*/

    updateDestiny(e){
        console.log("updateDestiny");
        console.log("Actualizando la propiedad destiny de person con el valor " + e.target.value);
        this.person.destiny = e.target.value;
    }


    updateDni(e){
        console.log("updateDni");
        console.log("Actualizando la propiedad dni de person con el valor " + e.target.value);
        this.person.dni = e.target.value;
    }

    updateName(e){
        console.log("updateName");
        console.log("Actualizando la propiedad name de person con el valor " + e.target.value);
        this.person.name = e.target.value;
    }

    updateCourse(e){
        console.log("updateCourse");
        console.log("Actualizando la propiedad course de person con el valor " + e.target.value);
        this.person.course = e.target.value;
    }

    updatePhone(e){
        console.log("updatePhone");
        console.log("Actualizando la propiedad phone de person con el valor " + e.target.value);
        this.person.phone = e.target.value;
    }

    updateEmail(e){
        console.log("updateEmail");
        console.log("Actualizando la propiedad email de person con el valor " + e.target.value);
        this.person.email = e.target.value;
    }
    

    goBack(e){
        console.log("newAtras en persona-form");
        e.preventDefault();
        this.resetFormData();
        this.dispatchEvent(new CustomEvent("persona-form-close",{}));
    }

    storePerson(e){
        console.log("storePerson");
        e.preventDefault();

/*        this.person.photo = {
            "src" : "./img/persona6.png",
            "alt" : "Persona"
        };
*/
        console.log("La propiedad name en person vale " + this.person.name);
        console.log("La propiedad profile en person vale " + this.person.profile);
        console.log("La propiedad yearsInCompany en person vale " + this.person.yearsInCompany);


        this.dispatchEvent(new CustomEvent("persona-form-store",{
                detail: {
                    person: {
                    destiny: this.person.destiny,
                    dni: this.person.dni,
                    name: this.person.name,
                    course: this.person.course,
                    phone: this.person.phone,
                    email: this.person.email
                    },
                    editingPerson: this.editingPerson
                }
            })
        );
    }

    resetFormData(){
        console.log("resetFormData");
        this.person = {};
        this.person.destiny = "";
        this.person.dni = "";
        this.person.name = "";
        this.person.course = "";
        this.person.phone = 0;
        this.person.email = "";

        this.editingPerson = false;
    }

}

customElements.define('persona-form', PersonaForm)

/*
<div class="form-group">
                        
<label for="destino">Elige un destino:</label>
    <select  name="destino" id="destino">
    <optgroup>
        <option value="playa">Playa</option>
        <option value="montaña">Montaña</option>
    </optgroup>

    </select>
</div>

*/