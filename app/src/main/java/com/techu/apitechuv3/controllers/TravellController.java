package com.techu.apitechuv3.controllers;

import com.techu.apitechuv3.models.StudentModel;
import com.techu.apitechuv3.models.TravellModel;
import com.techu.apitechuv3.services.StudentService;
import com.techu.apitechuv3.services.TravellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
@CrossOrigin(origins = "*" , methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,RequestMethod.DELETE})

public class TravellController {

    @Autowired
    TravellService travellService;

    @GetMapping("/travells")
    public List<TravellModel> getTravells(){
        System.out.println("getTravells");

        if(travellService.findAll().isEmpty()){
            TravellModel travel1 = new TravellModel("1", "Playa");
            TravellModel travel2 = new TravellModel("2", "Montaña");

            travellService.add(travel1);
            travellService.add(travel2);
        }

        return this.travellService.findAll();
    }

    @PostMapping("/travells")
    public ResponseEntity<TravellModel> addTravell(@RequestBody TravellModel travell){
        System.out.println("addTravell");
        System.out.println("La id del destino que se va a crear es: " + travell.getId());
        System.out.println("El destino que se va a crear es: " + travell.getDestiny());

        return new ResponseEntity<>(this.travellService.add(travell), HttpStatus.CREATED);
    }


    @PutMapping("/travells/{id}")
    public ResponseEntity<Object> updateTaravellById(@RequestBody TravellModel travell, @PathVariable String id){
        System.out.println("updateTravellById");
        System.out.println("La id en parámetro de la url es: " + id);
        System.out.println("La id del viaje que se quiere actualizar es: " + travell.getId());
        System.out.println("El curso del alumno que se va a actualizar es: " + travell.getDestiny());
        Optional<TravellModel> travellToUpdate = this.travellService.findById(id);

        if(travellToUpdate.isPresent()){
            if(id.equals(travell.getId())){
                return new ResponseEntity<>(this.travellService.update(travell), HttpStatus.OK);
            }else{
                return new ResponseEntity<>("No se puede cambiar el ID de un viaje", HttpStatus.BAD_REQUEST);
            }
        }else{
            return new ResponseEntity<>("Viaje  no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/travells/{id}")
    public ResponseEntity<String> deleteTravell(@PathVariable String id) {
        System.out.println("delteteTravell");
        System.out.println("La id del viajeborrar " + id);

        boolean deleteTravell = this.travellService.deleteById(id);

        if (deleteTravell == true) {
            return new ResponseEntity<>("Viaje borrado", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Viaje no encontrado para borrar", HttpStatus.NOT_FOUND);
        }
    }
}

