package com.techu.apitechuv3.controllers;

import com.techu.apitechuv3.models.StudentModel;
import com.techu.apitechuv3.models.TravellModel;
import com.techu.apitechuv3.services.StudentService;
import com.techu.apitechuv3.services.TravellService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@RestController
@RequestMapping("/apitechu/v3")
@CrossOrigin(origins = "*" , methods = { RequestMethod.GET, RequestMethod.POST, RequestMethod.PUT,RequestMethod.DELETE})

public class StudentController {

    @Autowired
    StudentService studentService;

    @Autowired
    TravellService travellService;

    @GetMapping("/students")
    public List<StudentModel> getStudents(){
        System.out.println("getStudents");

        return this.studentService.findAll();
    }

    @PostMapping("/students")
    public ResponseEntity<Object> addStudent (@RequestBody StudentModel student){
        System.out.println("addStudent");
        System.out.println("El dni del alumno que se va a crear es: " + student.getDni());
        System.out.println("El curso del alumno que se va a crear es: " + student.getCourse());
        System.out.println("El nombre del alumno que se va a crear es: " + student.getName());
        System.out.println("El email del alumno que se va a crear es: " + student.getEmail());
        System.out.println("El teléfono del alumno que se va a crear es: " + student.getPhone());


        Optional<StudentModel> result = this.studentService.findById(student.getDni());


        if (result.isPresent()) {
            return new ResponseEntity<>( "Alumno ya existe", HttpStatus.BAD_REQUEST);
        } else{
            for (TravellModel travellModel : this.travellService.findAll()) {
                if(travellModel.getDestiny().equals(student.getDestiny())){
                    return new ResponseEntity<>( this.studentService.add(student), HttpStatus.CREATED);
                }
            }
        }
        return new ResponseEntity<>( "Destino no existe", HttpStatus.NOT_FOUND);


    }

    @GetMapping("/students/{dni}")
    public ResponseEntity<Object> getStudentById(@PathVariable String dni){
        System.out.println("getStudentById");
        System.out.println("El dni el alumno a buscar " + dni);

        Optional<StudentModel> result = this.studentService.findById(dni);

        if (result.isPresent()){
            return new ResponseEntity<>(result.get(), HttpStatus.OK);
        }else{
            return new ResponseEntity<>( "Alumno no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @GetMapping("/students/destiny/{destiny}")
    public List<StudentModel> getStudentsByDestiny(@PathVariable String destiny){
        System.out.println("getStudentsByDestiny");
        System.out.println("El destino por parámetro es: " + destiny);

        List<StudentModel> studentInDestinyResult = new ArrayList<>();

        for (StudentModel student: this.studentService.findAll()){
            System.out.println("Estoy dentro del bucle for");
            System.out.println("El DNI del alumno es: " + student.getDni());
            if(destiny.equals(student.getDestiny())){
                studentInDestinyResult.add(student);
            }
        }
        return studentInDestinyResult;
    }

    @PutMapping("/students/{dni}")
    public ResponseEntity<Object> updateStudentById(@RequestBody StudentModel student, @PathVariable String dni){
        System.out.println("updateStudentById");
        System.out.println("La id en parámetro de la url es: " + dni);
        System.out.println("La id del alumno que se quiere actualizar es: " + student.getDni());
        System.out.println("El curso del alumno que se va a actualizar es: " + student.getCourse());
        System.out.println("El nombre del alumno que se va a actualizar es: " + student.getName());
        System.out.println("El email del alumno que se va a actualizar es: " + student.getEmail());
        System.out.println("El teléfono del alumno que se va a actualizar es: " + student.getPhone());

        Optional<StudentModel> studentToUpdate = this.studentService.findById(dni);

        if(studentToUpdate.isPresent()){
            if(dni.equals(student.getDni())){
                return new ResponseEntity<>(this.studentService.update(student), HttpStatus.OK);
            }else{
                return new ResponseEntity<>("No se puede cambiar el DNI de un alumno", HttpStatus.BAD_REQUEST);
            }
        }else{
            return new ResponseEntity<>("Alumno no encontrado", HttpStatus.NOT_FOUND);
        }
    }

    @DeleteMapping("/students/{dni}")
    public ResponseEntity<String> deleteStudent(@PathVariable String dni) {
        System.out.println("delteteStudent");
        System.out.println("La dni del alumno borrar " + dni);

        boolean deleteStudent = this.studentService.deleteById(dni);

        if (deleteStudent == true) {
            return new ResponseEntity<>("Alumno borrado", HttpStatus.OK);
        } else {
            return new ResponseEntity<>("Alumno no encontrado para borrar", HttpStatus.NOT_FOUND);
        }
    }


}
