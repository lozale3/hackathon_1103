package com.techu.apitechuv3.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

@Document (collection = "students")
public class StudentModel {

    @Id
    private String dni;
    private String name;
    private String email;
    private String course;
    private Integer phone;
    private String destiny;

    public StudentModel(String dni, String name, String email, String course, Integer phone, String destiny) {
        this.dni = dni;
        this.name = name;
        this.email = email;
        this.course = course;
        this.phone = phone;
        this.destiny = destiny;
    }

    public String getDni() {
        return dni;
    }

    public void setDni(String dni) {
        this.dni = dni;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getEmail() {
        return email;
    }

    public void setEmail(String email) {
        this.email = email;
    }

    public String getCourse() {
        return course;
    }

    public void setCourse(String course) {
        this.course = course;
    }

    public Integer getPhone() {
        return phone;
    }

    public void setPhone(Integer phone) {
        this.phone = phone;
    }

    public String getDestiny() {
        return destiny;
    }

    public void setDestiny(String destiny) {
        this.destiny = destiny;
    }
}
