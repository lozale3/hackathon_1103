package com.techu.apitechuv3.models;

import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Map;

@Document(collection = "travells")
public class TravellModel {

    @Id
    private String id;
    private String destiny;

    public TravellModel(String id, String destiny) {
        this.id = id;
        this.destiny = destiny;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getDestiny() {
        return destiny;
    }

    public void setDestiny(String destiny) {
        this.destiny = destiny;
    }
}
