package com.techu.apitechuv3.services;

import com.techu.apitechuv3.models.StudentModel;
import com.techu.apitechuv3.repositories.StudentRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class StudentService {

    @Autowired
    StudentRepository studentRepository;

    public List<StudentModel> findAll(){
        return this.studentRepository.findAll();
    }

    public StudentModel add(StudentModel student){
        System.out.println("add");

        return this.studentRepository.insert(student);
    }

    public Optional<StudentModel> findById(String dni){
        System.out.println("findByID");
        System.out.println("Obteniendo el alumno con la id " + dni);

        return this.studentRepository.findById(dni);
    }

    public boolean deleteById (String dni) {
        System.out.println("deleteById");
        boolean result = false;

        if(this.studentRepository.findById(dni).isPresent()){
            System.out.println("Borrando el alumno con la id " + dni);
            this.studentRepository.deleteById(dni);
            result = true;
        }
        return result;

    }

    public StudentModel update(StudentModel student){
        System.out.println("update");

        return this.studentRepository.save(student);
    }

 }
