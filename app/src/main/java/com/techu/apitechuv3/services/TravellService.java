package com.techu.apitechuv3.services;

import com.techu.apitechuv3.models.StudentModel;
import com.techu.apitechuv3.models.TravellModel;
import com.techu.apitechuv3.repositories.StudentRepository;
import com.techu.apitechuv3.repositories.TravellRepository;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.ArrayList;
import java.util.List;
import java.util.Optional;

@Service
public class TravellService {

    @Autowired
    TravellRepository travellRepository;

    public List<TravellModel> findAll() {

        System.out.println("findall");
        return this.travellRepository.findAll();
    }

    public TravellModel add(TravellModel travel) {
        System.out.println("add");

        return this.travellRepository.insert(travel);
    }

    public boolean deleteById (String id) {
        System.out.println("deleteById");
        boolean result = false;

        if(this.travellRepository.findById(id).isPresent()){
            System.out.println("Borrando el viaje con la id " + id);
            this.travellRepository.deleteById(id);
            result = true;
        }
        return result;

    }

    public TravellModel update(TravellModel travell){
        System.out.println("update");

        return this.travellRepository.save(travell);
    }


    public Optional<TravellModel>  findById(String id){
        System.out.println("findByID");
        System.out.println("Obteniendo el VIAJE con la id " + id);

        return this.travellRepository.findById(id);
    }

}
