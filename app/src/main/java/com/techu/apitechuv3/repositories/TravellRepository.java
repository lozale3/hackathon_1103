package com.techu.apitechuv3.repositories;

import com.techu.apitechuv3.models.StudentModel;
import com.techu.apitechuv3.models.TravellModel;
import org.springframework.data.mongodb.repository.MongoRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface TravellRepository extends MongoRepository<TravellModel, String> {
}
